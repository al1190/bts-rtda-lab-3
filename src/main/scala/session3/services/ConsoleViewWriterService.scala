package session3.services
import session3.models.View

/*
Classes in Scala are blueprints for creating objects.
They can contain methods, values, variables, types, objects, traits, and classes which are collectively called members.
In this case FileViewWriterService is overriding the functionality write(view: View) of his parent trait  ViewWriter
https://docs.scala-lang.org/tour/classes.html
*/
class ConsoleViewWriterService extends ViewWriter {
  override def write(view: View): Unit = {
    Console.println("--------------------")
    Console.println(s"${view.name()}:")
    Console.println()
    Console.println(view.toString())
    Console.println("--------------------")
    Console.println()
  }
}
