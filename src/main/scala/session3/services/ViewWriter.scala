package session3.services

import session3.models.{View}

/*
  Abstract types, such as traits and abstract classes, can in turn have abstract type members.
  This means that the concrete implementations (FileViewWriterService, ConsoleViewWriterService)
  define the actual types.
  https://docs.scala-lang.org/tour/abstract-type-members.html
*/
trait ViewWriter {
  def write(view: View): Unit
}
