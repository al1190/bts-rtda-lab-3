package session3.models

/*
  Case classes are good for modeling immutable data.
  Once you create a MostCommonBirdView object it will never change.
  MostCommonBirdView is modeling an pre-calculated view of the data.
  It's overriding the functionalities of the abstract class View
  https://docs.scala-lang.org/tour/case-classes.html
*/
case class MostCommonBirdView(birdId: Int, count: Int) extends View {

  override def toString: String = {
    s"Most Common Bird: ($birdId, $count)"
  }

  override def name(): String = "most_common_bird_view"
}
